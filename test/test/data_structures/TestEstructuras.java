package test.data_structures;



import model.data_structures.Queue;
import model.data_structures.Stack;
import model.data_structures.iterator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestEstructuras {

	private Queue<prueba> queue;
	private Stack<prueba> stack;
	
	@Before
	public void setUp1() {
		queue= new Queue<>();
		stack= new Stack<>();
	}

	public void setUp2() {
		for(int i =0; i< 10; i++)
		{
			prueba p = new prueba(i);
			queue.enqueue(p);
			stack.push(p);
			
		}
	}

	@Test
	public void testEstructuras() {
		
		assertTrue(queue!=null);
		assertEquals(0, queue.size());
		assertEquals(0, stack.size());
		System.out.println(""+queue.size()+"::"+stack.size());
	}

	@Test
	public void testEstructuras2() {
		setUp2();
		
		assertEquals(10, queue.size());
		assertEquals(10, stack.size());
		System.out.println(""+queue.size()+"::"+stack.size());
	}
	@Test
	public void AgregarTest(){
		
		for(int i=1;i<5;i++)
		{
			prueba temp = new prueba(i);
			queue.enqueue(temp);
			stack.push(temp);
			assertEquals(i, queue.size());
			assertEquals(i, stack.size());
		}
		assertEquals(4, queue.size());
		assertEquals(4, stack.size());
	}
	
	@Test
	public void EliminarTest(){
		
		setUp2();
		iterator<prueba> iter = new iterator<>(queue.getHead());
		int tama�o = queue.size();
		while(iter.hasNext())
		{
			iter.next();
			stack.pop();
			queue.dequeue();
			tama�o--;
			assertEquals(tama�o, queue.size());
			assertEquals(tama�o, stack.size());
		}
		assertEquals(0, queue.size());
		assertEquals(0, stack.size());
	}
}
