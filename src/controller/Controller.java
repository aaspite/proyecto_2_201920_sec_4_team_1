package controller;

import java.util.Scanner;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
	/**
	 *  metodo Run
	 */
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				System.out.println("Ingrese el limite de carga (si desea todos los datos ingrese 0): ");
				int limite = lector.nextInt();
				modelo.cargar(limite); 	
				System.out.println("Datos cargados :)");
				break;
			case 2:
				System.out.println("Ingrese N: ");
				int n = lector.nextInt();
				modelo.obtenerNLetrasMasFrecuentes(n);
				
				break;
			case 3:
				System.out.println("Ingrese la latitud: ");
				String latitud = lector.next();
				System.out.println("Ingrese la longitud: ");
				String longitud = lector.next();

				modelo.buscarNodosDelimitanZonas(Double.parseDouble(latitud), Double.parseDouble(longitud));
				
				break;
			case 4:
				System.out.println("Ingrese el limite bajo: ");
				double bajo = lector.nextInt();
				System.out.println("Ingrese el limite alto: ");
				double alto = lector.nextInt();
				System.out.println("Ingrese N: ");
				int nViajes = lector.nextInt();
				modelo.buscarTiempoPromedioViajeRango(bajo, alto,nViajes);
				break;
			case 5:
				System.out.println("Ingrese N: ");
				int n5 = lector.nextInt();
				modelo.buscarNZonasMasNorte(n5);
				break;
			case 6:
				System.out.println("Ingrese la latitud: ");
				String lat = lector.next();
				System.out.println("Ingrese la longitud: ");
				String longi = lector.next();
				modelo.buscarNodosMallaVial(Double.parseDouble(lat), Double.parseDouble(longi));
				break;
			case 7:
				System.out.println("Ingrese el limite bajo: ");
				int baj = lector.nextInt();
				System.out.println("Ingrese el limite alto: ");
				int alt = lector.nextInt();
				System.out.println("Ingrese N: ");
				int n7 = lector.nextInt();
				modelo.buscarDesvViajeRango(baj, alt, n7);
				break;
			case 8:
				System.out.println("Ingrese el id de la zona salida: ");
				int idZonaSalida = lector.nextInt();
				System.out.println("Ingrese la hora: ");
				int hora = lector.nextInt();

				modelo.buscarTiemposZonaSalidaYHora(idZonaSalida, hora);
				break;
			case 9:
				System.out.println("Ingrese el id de la zona salida: ");
				int idZonaLlegada = lector.nextInt();
				System.out.println("Ingrese la hora inicio: ");
				int horaInicio = lector.nextInt();
				System.out.println("Ingrese la hora fin: ");
				int horaFin = lector.nextInt();
				modelo.buscarTiemposZonaLlegadaYRangoHoras(idZonaLlegada, horaInicio, horaFin);
				break;
			case 10:
				System.out.println("Ingrese N: ");
				int n10 = lector.nextInt();
				modelo.buscarNZonasMasNodos(n10);
				break;
			case 11: 
				modelo.generarGraficaDatosFaltantes();
				break;
			case 12: 
				System.out.println("--------- \n Hasta pronto !! \n---------"); 
				lector.close();
				fin = true;
				break;	

			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
