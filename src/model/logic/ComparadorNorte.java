package model.logic;

import java.util.Comparator;

public class ComparadorNorte implements Comparator<Zona>{
		
		public ComparadorNorte(){
			
		}

		@Override
		public int compare(Zona zona1, Zona zona2) {
			
			Double[] altitudesZona1 = zona1.getCoordenadas()[0];
			Double[] altitudesZona2 = zona2.getCoordenadas()[0];
			
			boolean isHigherThanAll = false;
			
			for (int i = 0; i < altitudesZona1.length && !isHigherThanAll; i++) {
				Double currentAltitudZona1 = altitudesZona1[i];
				double max = 0;
				for (int j = 0; j < altitudesZona2.length; j++) {
						Double currentAltitudZona2 = altitudesZona2[j];
						if(currentAltitudZona2 > max){
							max = currentAltitudZona2;
						}
				}
				if(currentAltitudZona1 > max){
					isHigherThanAll = true;
				}
			}
			
			
			return isHigherThanAll ? 1 : -1;
		}
}

