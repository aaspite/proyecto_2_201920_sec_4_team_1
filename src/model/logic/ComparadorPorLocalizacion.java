package model.logic;
import java.util.Comparator;
import java.text.DecimalFormat;
public class ComparadorPorLocalizacion implements Comparator<Viaje>
{
	public int compare(Viaje v1, Viaje v2) {
	
		
		
		if ( (double)Math.round(v1.getLatitud()* 1000d)/1000> (double)Math.round(v2.getLatitud()* 1000d)/1000)
			return 1;
		else if ( (double)Math.round(v1.getLatitud()* 1000d)/1000<(double)Math.round(v2.getLatitud()* 1000d)/1000)
			return -1;
		else
		{
			if( (double)Math.round(v1.getLongitud()* 1000d)/1000> (double)Math.round(v2.getLongitud()* 1000d)/1000)
				return 1;
			else if( (double)Math.round(v1.getLongitud()* 1000d)/1000<(double)Math.round(v2.getLongitud()* 1000d)/1000)
				return -1;
			else
				return 0;
		}
	}
}
