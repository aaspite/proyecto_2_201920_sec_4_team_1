package model.logic;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.Iterator;

import javax.print.attribute.standard.Sides;

import model.data_structures.ArregloDinamico;
import model.data_structures.BinarySearchTree;
import model.data_structures.MaxPQ;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateChainingHashST;


import com.google.gson.*;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {

	/*
	 * ATRIBUTOS
	 */
	private ArregloDinamico<Viaje> viajesMes = new ArregloDinamico<Viaje>();
	private RedBlackBST<Integer, Nodo> nodos = new RedBlackBST<Integer, Nodo>();

	/*
	 * Atributos usados por el esutudiante A
	 * 
	 */
	SeparateChainingHashST<Double, Queue<Viaje>> hash = new SeparateChainingHashST<>();
	// private RedBlackBST<Hora, Queue<Viaje>> tree;

	// comparadores para las zonas
	private Comparator<Zona> comparadorZonasLetras = new ComparadorZonasLetras();
	private Comparator<Zona> comparadorZonasNorte = new ComparadorNorte();
	private Comparator<Zona> comparadorZonasMasNodos = new ComparadorZonasMasNodos();

	// comparador para viajes por hora segun su "source id"
	private Comparator<Viaje> ComparadorViajesPorHora = new ComparadorViajesPorHora();

	// Priority Queues para las zonas
	private MaxPQ<Zona> zonasPorLetras;

	// Priority Queues para los viajes por hora
	private MaxPQ<Viaje> viajesHoraPorSourceId;

	/**
	 * trimestre
	 */
	

	/*
	 * METODOS
	 */

	/**
	 * Carga los archivos de los Viajes
	 * 
	 * @param trimestre,
	 *            trimestre del año
	 */
	public void cargar( int limite) {
		System.out.println("Cargando...");

		String line = "";
		int sizeMalla = 0;
		File fileMalla = new File("./data/Nodes_of_red_vial-wgs84_shp.txt");
		try (BufferedReader br = new BufferedReader(new FileReader(fileMalla))) {
			br.readLine();
			line = br.readLine();
			while (line != null && (limite > 0 ? sizeMalla < limite : true)) {
				String[] data = line.split(",");
				// id del nodo (key)
				int nodeId = Integer.parseInt(data[0]);

				// longitud
				double longitud = Double.parseDouble(data[1]);
				// latitud
				double latitud = Double.parseDouble(data[2]);

				sizeMalla++;
				Nodo nodoActual = new Nodo(nodeId, longitud, latitud);
				// nodosHash.put(nodeId, nodoActual);
				nodos.put(nodeId, nodoActual);
				line = br.readLine();
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// importar json

		int sizeZonas = 0;

		zonasPorLetras = new MaxPQ<Zona>(comparadorZonasLetras);
		@SuppressWarnings("deprecation")
		JsonParser parser = new JsonParser();
		File datos = new File("./data/bogota_cadastral.json");

		try {
			@SuppressWarnings("deprecation")
			JsonObject obj = (JsonObject) parser.parse(new FileReader(datos));
			String ptype = obj.get("type").getAsString();

			JsonArray arr = (JsonArray) obj.get("features").getAsJsonArray();
			for (int i = 0; arr != null && i < arr.size(); i++) {

				JsonObject aux = (JsonObject) arr.get(i);

				String ptype2 = aux.get("type").getAsString();

				JsonObject pgeometry = (JsonObject) aux.get("geometry");

				JsonArray coordinatesJson = pgeometry.get("coordinates").getAsJsonArray();

				JsonArray coordinatesJson2 = (JsonArray) ((JsonArray) coordinatesJson.get(0)).get(0);

				Double[][] coordinates = new Double[2][coordinatesJson2.size()];

				for (int j = 0; j < coordinatesJson2.size(); j++) {
					JsonArray current = (JsonArray) coordinatesJson2.get(j);
					coordinates[0][j] = current.get(1).getAsDouble();
					coordinates[1][j] = current.get(0).getAsDouble();
				}

				JsonObject properties = (JsonObject) aux.get("properties");

				int movementId = properties.get("MOVEMENT_ID").getAsInt();
				String scanombre = properties.get("scanombre").getAsString();
				double shape_leng = properties.get("shape_leng").getAsDouble();
				double shape_area = properties.get("shape_area").getAsDouble();

				Zona zona = new Zona(movementId, scanombre, shape_leng, shape_area, coordinates);
				zonasPorLetras.insert(zona);

				sizeZonas++;
			}
		} catch (JsonIOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonSyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		viajesHoraPorSourceId = new MaxPQ<Viaje>(ComparadorViajesPorHora);

		
			String line1 = "";
			int sizeMonth = 0;
			int sizehour = 0;
			int sizeHour=0;
			File fileHour = new File("./data/bogota-cadastral-2018-" + 1 + "-All-HourlyAggregate.csv");
			try (BufferedReader br = new BufferedReader(new FileReader(fileHour))) {
				br.readLine();
				line1 = br.readLine();
				while (line1 != null && (limite > 0 ? sizehour < limite : true)) {
					String[] data = line1.split(",");
					int dato1 = Integer.parseInt(data[0]);
					int dato2 = Integer.parseInt(data[1]);
					int dato3 = Integer.parseInt(data[2]);
					double dato4 = Double.parseDouble(data[3]);
					double dato5 = Double.parseDouble(data[4]);
					double dato6 = Double.parseDouble(data[5]);
					double dato7 = Double.parseDouble(data[6]);
					Viaje Viaje = new Viaje(dato1, dato2, dato3, dato4, dato5, dato6, dato7);
					viajesHoraPorSourceId.insert(Viaje);
					System.out.println(Viaje.getSourceID()+"   "+Viaje.getTime());
					sizehour++;
					line1 = br.readLine();
				}
				br.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String line2 = "";
			File fileHour2 = new File("./data/bogota-cadastral-2018-" + 2 + "-All-HourlyAggregate.csv");
			try (BufferedReader br = new BufferedReader(new FileReader(fileHour2))) {
				br.readLine();
				line2 = br.readLine();
				while (line2 != null && (limite > 0 ? sizehour < limite : true)) {
					String[] data = line2.split(",");
					int dato1 = Integer.parseInt(data[0]);
					int dato2 = Integer.parseInt(data[1]);
					int dato3 = Integer.parseInt(data[2]);
					double dato4 = Double.parseDouble(data[3]);
					double dato5 = Double.parseDouble(data[4]);
					double dato6 = Double.parseDouble(data[5]);
					double dato7 = Double.parseDouble(data[6]);
					Viaje Viaje = new Viaje(dato1, dato2, dato3, dato4, dato5, dato6, dato7);
					//viajesHora.put(Viaje.getSourceID() + "-" + Viaje.getdsId() + "-" + Viaje.getTime(), Viaje);
					viajesHoraPorSourceId.insert(Viaje);
					
					sizehour++;
					line2 = br.readLine();
				}
				br.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("Cargue de Viajes por hora terminado");

			File fileMonthly = new File("./data/bogota-cadastral-2018-" + 1 + "-All-MonthlyAggregate.csv");
			try (BufferedReader br3 = new BufferedReader(new FileReader(fileMonthly))) {
				br3.readLine();
				line = br3.readLine();
				while (line != null && (limite > 0 ? sizeMonth < limite : true)) {
					String[] data = line.split(",");
					int dato1 = Integer.parseInt(data[0]);
					int dato2 = Integer.parseInt(data[1]);
					int dato3 = Integer.parseInt(data[2]);
					double dato4 = Double.parseDouble(data[3]);
					double dato5 = Double.parseDouble(data[4]);
					double dato6 = Double.parseDouble(data[5]);
					double dato7 = Double.parseDouble(data[6]);
					Viaje Viaje = new Viaje(dato1, dato2, dato3, dato4, dato5, dato6, dato7);
					viajesMes.append(Viaje);
					
					sizeMonth++;
					line = br3.readLine();
				}
				br3.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println("Cargue de Viajes por mes terminado");
			System.out.println("Total de Viajes en el archivo mes del trimestre " + 1 + " = " + sizeMonth +"\n"
					+"Total de Viajes en los archivo hora del trimestre 1 y 2 = " + sizehour +"\n"
					+"Número de nodos de la malla vial (archivo .txt): " + sizeMalla+"\n"
					+"Numero de zonas cargadas (archivo json): " + sizeZonas);

		}
	

	// -------------
	// PARTE A
	// -------------
	/**
	 * 1A
	 * Obtiene las n letras mas frecuentes por las que empieza un nombre de una
	 * Zona
	 * 
	 * @param n,
	 *            numero de letras solicitadas
	 */
	public void obtenerNLetrasMasFrecuentes(int n) {
		// first create the new PQ

		Iterator<Zona> iteradorZonasPorLetras = zonasPorLetras.iterator();

		MaxPQ<Letra> letras = new MaxPQ<Letra>();

		int contadorLetrasRepetidas = 0;
		String zonasLetraRepetida = "";

		Zona pastZona = null;

		while (iteradorZonasPorLetras.hasNext()) {

			Zona currentZona = iteradorZonasPorLetras.next();

			char pastChar = ' ';

			if (pastZona != null) {
				pastChar = pastZona.getScaNombre().charAt(0);
			}

			if (Character.compare(pastChar, currentZona.getScaNombre().charAt(0)) != 0) {
				letras.insert(new Letra(pastChar, contadorLetrasRepetidas, zonasLetraRepetida));
				contadorLetrasRepetidas = 0;
				zonasLetraRepetida = "";
			} else {
				zonasLetraRepetida += currentZona.getScaNombre() + ", ";
				contadorLetrasRepetidas++;
			}

			pastZona = currentZona;
		}

		Iterator<Letra> iteradorLetras = letras.iterator();
		int contadorLetras = 0;

		while (iteradorLetras.hasNext() && contadorLetras < n) {

			Letra currentLetra = iteradorLetras.next();
			System.out.println("LETRA " + (contadorLetras + 1) + ": " + currentLetra.getLetra() + "\n"
					+ "Número de repeticiones: " + currentLetra.getReps());
			System.out.println("Zonas: \n" + currentLetra.getZonasLetraRepetida() +"\n");
			contadorLetras++;
		}

	}
	/**
	 * 2A
	 * @param altitud
	 * @param longitud
	 */
	public void buscarNodosDelimitanZonas(double altitud, double longitud) {

		// add nodes from zonas to tree

		Iterator<Zona> iterador = zonasPorLetras.iterator();
		RedBlackBST<Integer, Nodo> arbolNodos = new RedBlackBST<Integer, Nodo>();

		while (iterador.hasNext()) {

			Zona currentZona = iterador.next();

			for (int i = 0; i < currentZona.getCoordenadas().length; i++) {
				double alti = currentZona.getCoordenadas()[0][i];
				double longi = currentZona.getCoordenadas()[1][i];
				Nodo currentNodo = new Nodo(currentZona.getMovementId(), longi, alti);
				arbolNodos.put(currentZona.getMovementId(), currentNodo);
			}

		}

		// loop the tree the same way as a 2B, but compare with 3 decimal

		Iterable<Integer> keys = arbolNodos.keys();

		Iterator<Integer> iteradorLlaves = keys.iterator();

		Nodo parametro = new Nodo(0, longitud, altitud);

		while (iteradorLlaves.hasNext()) {
			Integer currentKey = iteradorLlaves.next();
			Nodo currentNodo = arbolNodos.get(currentKey);

			if (currentNodo.compareToWith3Decimals(parametro) == 0) {
				System.out.println("\n" + "Id: " + currentNodo.getId() + "\n" + "Latitud: " + currentNodo.getLatitud() + "\n" + "Longitud: " + currentNodo.getLongitud());
			}
		}
		System.out.println("\n");
	}

	/**
	 * pre: el arreglo debe estar ordenado
	 * 3A
	 * @param t1
	 * @param t2
	 */
	public void buscarTiempoPromedioViajeRango(double t1, double t2, int nViajes) {
		
			// Insertar al hashis
			SeparateChainingHashST<Double, Viaje> hashis = new SeparateChainingHashST<>();
			int i = 0;
			while (i < viajesMes.size()) {
				hashis.put(viajesMes.get(i).getMeanTravelTime(), viajesMes.get(i));
				i++;
			}
			int limitante=nViajes;
			Iterator<Double> desvs = hashis.keys().iterator();
//		
			ArregloDinamico<Viaje> retorno = new ArregloDinamico<>();
			
			while (desvs.hasNext()) {
				double viaje = desvs .next();
				Viaje element = hashis.get(viaje);
				if(element.getMeanTravelTime()>t1 && element.getMeanTravelTime()<t2 )
				{
					retorno.append(element);
					
				}
			}
			ordenarViajesShellSort(retorno);
			if(retorno.size()<nViajes)
			{
				limitante=retorno.size();
			}
			if(retorno.size()==0)
			{
				System.out.println("No hay Viajes en ese rango");
			}
			else
			{
			for(int l=0;l<limitante;l++)
			{
				System.out.println("Zona de Origen: " + retorno.get(l).getSourceID()+"\n"
						+"Zona de Destino: " + retorno.get(l).getdsId()+"\n"
						+"Mes: " + retorno.get(l).getTime()+"\n"
						+ "Tiempo promedio: " + retorno.get(l).getMeanTravelTime()+"\n");
			}
			}
	}
	
	// -------------
	// PARTE B
	// -------------

	// recorre la cola de prioridad hecha con el comparator que las ordena segun
	// que tan al norte se encuentran
	/**
	 * 1B
	 * @param n
	 */
	public void buscarNZonasMasNorte(int n) {

		// first create the new PQ

		MaxPQ<Zona> zonasMasAlNorte = new MaxPQ<Zona>(comparadorZonasNorte);

		Iterator<Zona> iteradorPorLetras = zonasPorLetras.iterator();

		while (iteradorPorLetras.hasNext()) {

			Zona currentZona = iteradorPorLetras.next();
			zonasMasAlNorte.insert(currentZona);
		}

		Iterator<Zona> iterador = zonasMasAlNorte.iterator();

		int counter = 0;

		while (iterador.hasNext() && counter < n) {

			Zona currentZona = iterador.next();
			System.out.println("Zona #" + (counter + 1) + ": " + currentZona.getScaNombre());
			System.out.println("Punto mas al norte: ");

			Double[][] coordenadas = currentZona.getCoordenadas();
			Double[] altitudes = coordenadas[0];

			double maxAltitud = altitudes[0];
			double longitudOfMaxAltitud = coordenadas[1][0];

			for (int i = 0; i < altitudes.length; i++) {
				if (altitudes[i] > maxAltitud) {
					maxAltitud = altitudes[i];
					longitudOfMaxAltitud = coordenadas[1][i];
				}
			}

			System.out.println("Altitud: " + maxAltitud);
			System.out.println("Longitud: " + longitudOfMaxAltitud + "\n");
			counter++;
		}
		System.out.println("FIN \n");
	}
	/**
	 * 2B
	 * @param latitud
	 * @param longitud
	 */
	public void buscarNodosMallaVial(double latitud, double longitud) {

		Iterable<Integer> keys = nodos.keys();

		Iterator<Integer> iterador = keys.iterator();

		Nodo parametro = new Nodo(0, longitud, latitud);

		while (iterador.hasNext()) {
			Integer currentKey = iterador.next();
			Nodo currentNodo = nodos.get(currentKey);
			if (currentNodo.compareTo(parametro) == 0) {
				System.out.println("\n" + "Id: " + currentNodo.getId() + "\n" + "Latitud: " + currentNodo.getLatitud() + "\n" + "Longitud: " + currentNodo.getLongitud());
			}
		}

	}
	/**
	 * 3B
	 * @param d1
	 * @param d2
	 * @param nViajes
	 */
	public void buscarDesvViajeRango(double d1, double d2,int nViajes) {

		// Insertar al hashis
		SeparateChainingHashST<Double, Viaje> hashis = new SeparateChainingHashST<>();
		int i = 0;
		while (i < viajesMes.size()) {
			hashis.put(viajesMes.get(i).getStandardDeviation(), viajesMes.get(i));
			i++;
		}
		int limitante=nViajes;
		Iterator<Double> desvs = hashis.keys().iterator();
//	
		ArregloDinamico<Viaje> retorno = new ArregloDinamico<>();
		
		while (desvs.hasNext()) {
			double viaje = desvs .next();
			Viaje element = hashis.get(viaje);
			if(element.getStandardDeviation()>d1 && element.getStandardDeviation()<d2 )
			{
				retorno.append(element);
				
			}
		}
		ordenarViajesShellSort(retorno);
		if(retorno.size()<nViajes)
		{
			limitante=retorno.size();
		}
		
		if(retorno.size()==0)
		{
			System.out.println("No hay viajes en ese rango");
		}
		else{
		for(int l=0;l<limitante;l++)
		{
			System.out.println("Zona de Origen: " + retorno.get(l).getSourceID()+"\n"
					+"Zona de Destino: " + retorno.get(l).getdsId()+"\n"
					+"Mes: " + retorno.get(l).getTime()+"\n"
					+ "Desviacion: " + retorno.get(l).getStandardDeviation()+"\n");
		}
		}
	}
	// -------------
	// PARTE C
	// -------------
	/**
	 * 1C
	 * @param idZona
	 * @param hora
	 */
	public void buscarTiemposZonaSalidaYHora(int idZona, int hora) {
		
		
	Iterator<Viaje> iteradorViajesHoraPorSourceId = viajesHoraPorSourceId.iterator();
	while(iteradorViajesHoraPorSourceId.hasNext())
				{
		Viaje tempo= iteradorViajesHoraPorSourceId.next();
					 
					if(tempo.getSourceID()== idZona && tempo.getTime()== hora)
					{
						System.out.println("Zona Origen: "+tempo.getSourceID()+"\n"
								+"Zona Destino: "+tempo.getdsId()+"\n"
								+"Tiempo promedio: "+tempo.getMeanTravelTime());
					}
				}
	}

	public void buscarTiemposZonaLlegadaYRangoHoras(int idZona, int horaInicio, int horaFin) {

		Iterator<Viaje> iteradorViajesHoraPorSourceId = viajesHoraPorSourceId.iterator();
		while(iteradorViajesHoraPorSourceId.hasNext())
					{
			Viaje tempo= iteradorViajesHoraPorSourceId.next();
						 
						if(tempo.getdsId()== idZona && horaInicio<=tempo.getTime() && tempo.getTime()<= horaFin)
						{
							System.out.println("Zona Origen: "+tempo.getSourceID()+"\n"
									+"Zona Destino: "+tempo.getdsId()+"\n"
									+"Tiempo promedio: "+tempo.getMeanTravelTime());
						}
					}
		}
public void buscarNZonasMasNodos(int n) {

	// first create the new PQ

	MaxPQ<Zona> zonasMasNodos = new MaxPQ<Zona>(comparadorZonasMasNodos);

	Iterator<Zona> iterador = zonasPorLetras.iterator();

	while (iterador.hasNext()) {

		Zona currentZona = iterador.next();
		zonasMasNodos.insert(currentZona);
	}

	// then get the n highest elements

	Iterator<Zona> iteradorZonasMasNodos = zonasMasNodos.iterator();
	int counter = 0;

	while (iteradorZonasMasNodos.hasNext() && counter < n) {

		Zona currentZona = iteradorZonasMasNodos.next();
		System.out.println("Zona #" + (counter + 1) + ": " + currentZona.getScaNombre());
		System.out.println("Número de Nodos: " + currentZona.getCoordenadas()[0].length + "\n");
		counter++;
	}

	System.out.println("FIN \n");
}

public void generarGraficaDatosFaltantes() {

	//datos para trimestres 1 y 2
	System.out.println("Porcentaje de datos faltantes");
	Iterator<Viaje> iteradorViajesHoraPorSourceId = viajesHoraPorSourceId.iterator();

	Viaje pastViaje = null;
	int contadorViajesPorSourceId = 0;
	String textoPorcentajeViajesFaltantes = "";
	int pastId = 0;
	//int pastDestId = 0;
	float porcentajeFinal = 0;
	int cantidadPuntos = 0;
	//int pastTime = 0;

	int viajesPorZona = 1160*24*2;

	while (iteradorViajesHoraPorSourceId.hasNext()) {
		Viaje currentViaje = iteradorViajesHoraPorSourceId.next();


		if(pastViaje != null){
			pastId = pastViaje.getSourceID();
			//pastDestId = pastViaje.getdsId();
			//pastTime = pastViaje.getTime();
		}

		//if(pastId - currentViaje.getSourceID() == 0 && (pastDestId - currentViaje.getdsId() == 0 ^ pastTime - currentViaje.getTime() == 0 )){
		if(pastId - currentViaje.getSourceID() == 0){
			contadorViajesPorSourceId++;

		} else {
			if(pastId != 0){
				porcentajeFinal = (float) ((viajesPorZona-contadorViajesPorSourceId)*100 / viajesPorZona);
				cantidadPuntos = (int) (porcentajeFinal / 2) ;
				for (int i = 0; i < cantidadPuntos; i++) {
					textoPorcentajeViajesFaltantes += "*";
				}

				System.out.println(pastId + " | " + textoPorcentajeViajesFaltantes);
				textoPorcentajeViajesFaltantes = "";
				contadorViajesPorSourceId = 0;
			}
		}

		pastViaje = currentViaje;
	}


}

//imprimir tabla

// -------------
// METODOS PRIVADOS
// -------------

// Verificadores para prevenir errores durante la ejecucion



private boolean verificarValidezDia(int dia) {

	boolean verificacion = true;

	if (dia > 7) {
		verificacion = false;
	} else if (dia < 1) {
		verificacion = false;
	}

	return verificacion;
}

private boolean verificarValidezHora(int hora) {

	boolean verificacion = true;

	if (hora > 24) {
		verificacion = false;
	} else if (hora < 0) {
		verificacion = false;
	}

	return verificacion;
}

// Ordenamiento

/**
 * compara si un Viaje dado es menor que el otro
 * 
 * @param v
 *            Viaje a comparar
 * @param w
 *            Viaje a comprarar
 * @return true si w es mayor que v
 */
private static boolean less(Viaje v, Viaje w) {
	return v.compareTo(w) < 0;
}

/**
 * intercambia dos Viajes de posicion
 * 
 * @param array
 *            arreglo de donde se van a ahcer los intercambios
 * @param i
 *            posicion del elemento a cambiar
 * @param j
 *            posicion del elememento a intercambiar
 */
private static void exch(ArregloDinamico<Viaje> array, int i, int j) {
	Viaje swap = array.get(i);
	array.replace(array.get(j), i);
	array.replace(swap, j);
}

/**
 * Ordena los Viajes en orden ascendente los Viajes
 * 
 * @param ViajesAOrdenar
 *            arreglo a ordenar
 */
public void ordenarViajesShellSort(ArregloDinamico<Viaje> ViajesAOrdenar) {

	// Empezar a medir el tiempo
	long startTimeShellSort = System.currentTimeMillis();

	int N = ViajesAOrdenar.size();
	int h = 1;

	while (h < N / 3) {
		h = 3 * h + 1;
	}

	while (h >= 1) {
		for (int i = h; i < N; i++) {
			for (int j = i; j >= h && less(ViajesAOrdenar.get(j), ViajesAOrdenar.get(j - h)); j -= h) {
				exch(ViajesAOrdenar, j, j - h);
			}
		}
		h = h / 3;
	}

	// Terminar de medir el tiempo
	long endTimeShellSort = System.currentTimeMillis();

}

}