package model.logic;

import java.util.Comparator;

public class ComparadorZonasMasNodos implements Comparator<Zona>{
		
		public ComparadorZonasMasNodos(){
			
		}

		@Override
		public int compare(Zona zona1, Zona zona2) {
			
			Double[] nodosZona1 = zona1.getCoordenadas()[0];
			Double[] nodosZona2 = zona2.getCoordenadas()[0];
			
			if(nodosZona1.length > nodosZona2.length){
				return 1;
			} else if (nodosZona1.length < nodosZona2.length){
				return -1;
			} else {
				return 0;
			}
		}
}

