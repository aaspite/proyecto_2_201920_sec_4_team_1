package model.logic;
import java.util.Comparator;
import java.text.DecimalFormat;
public class ComparadorNodos implements Comparator<Nodo>
{
	public int compare(Nodo v1, Nodo v2) {
		
		if ( (double)Math.round(v1.getLatitud()* 100d)/100 > (double)Math.round(v2.getLatitud()* 100d)/100)
			return 1;
		else if ( (double)Math.round(v1.getLatitud()* 100d)/100<(double)Math.round(v2.getLatitud()* 100d)/100)
			return -1;
		else
		{
			if( (double)Math.round(v1.getLongitud()* 100d)/100 > (double)Math.round(v2.getLongitud()* 100d)/100)
				return 1;
			else if( (double)Math.round(v1.getLongitud()* 100d)/100<(double)Math.round(v2.getLongitud()* 100d)/100)
				return -1;
			else
				return 0;
		}
	}
}
