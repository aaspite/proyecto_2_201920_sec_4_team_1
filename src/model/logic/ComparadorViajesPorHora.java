package model.logic;
import java.util.Comparator;

public class ComparadorViajesPorHora implements Comparator<Viaje>
{
	public int compare(Viaje v1, Viaje v2) {
	
		if(v1.getSourceID() < v2.getSourceID()){
			return 1;
		} else if(v1.getSourceID() > v2.getSourceID()){
			return -1;
		} else if(v1.getdsId() < v2.getdsId()){
			return 1;
		} else if(v1.getdsId() > v2.getdsId()){
			return -1;
		} else {
			return 0;
		}
	}
}
