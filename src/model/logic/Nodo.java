package model.logic;

public class Nodo {

	//---------
	//Atributos
	//---------
	
	/**
	 * ID del nodo
	 */
	private int id;
	
	/**
	 *  longitud, latitud
	 */
	private double longitud, latitud;
	
	/**
	 *  coordenadas
	 */
	//private Double[][] coordenadas;
	
	public Nodo(int p1,double p2,double p3){
		id=p1;
		longitud=p2;
		latitud=p3;
	}
	
	public int getId()
	{
		return id;
	}
	
	public double getLongitud()
	{
		return longitud;
	}
	
	public double getLatitud()
	{
		return latitud;
	}
	
	/**
	public Double[][] getCoordenadas()
	{
		return coordenadas;
	}
	*/

	public int compareTo(Nodo other) {
		//System.out.println((double)Math.round(this.getLatitud()* 100d)/100  + ">"+  (double)Math.round(other.getLatitud()* 100d)/100);
		//System.out.println((double)Math.round(this.getLatitud()* 100d)/100 > (double)Math.round(other.getLatitud()* 100d)/100);
		//System.out.println((double)Math.round(this.getLatitud()* 100d)/100 + "<" + (double)Math.round(other.getLatitud()* 100d)/100);
		//System.out.println((double)Math.round(this.getLatitud()* 100d)/100<(double)Math.round(other.getLatitud()* 100d)/100);
		//System.out.println((double)Math.round(this.getLongitud()* 100d)/100 +">"+ (double)Math.round(other.getLongitud()* 100d)/100);
		//System.out.println((double)Math.round(this.getLongitud()* 100d)/100 > (double)Math.round(other.getLongitud()* 100d)/100);
		//System.out.println((double)Math.round(this.getLongitud()* 100d)/100 + "<" +(double)Math.round(other.getLongitud()* 100d)/100);
		//System.out.println((double)Math.round(this.getLongitud()* 100d)/100<(double)Math.round(other.getLongitud()* 100d)/100);
		if ( (double)Math.round(this.getLatitud()* 100d)/100 > (double)Math.round(other.getLatitud()* 100d)/100)
			return 1;
		else if ( (double)Math.round(this.getLatitud()* 100d)/100<(double)Math.round(other.getLatitud()* 100d)/100)
			return -1;
		else
		{
			if( (double)Math.round(this.getLongitud()* 100d)/100 > (double)Math.round(other.getLongitud()* 100d)/100)
				return 1;
			else if( (double)Math.round(this.getLongitud()* 100d)/100<(double)Math.round(other.getLongitud()* 100d)/100)
				return -1;
			else
				return 0;
		}

	}
	
	public int compareToWith3Decimals(Nodo other) {
		//System.out.println((double)Math.round(this.getLatitud()* 100d)/100  + ">"+  (double)Math.round(other.getLatitud()* 100d)/100);
		//System.out.println((double)Math.round(this.getLatitud()* 100d)/100 > (double)Math.round(other.getLatitud()* 100d)/100);
		//System.out.println((double)Math.round(this.getLatitud()* 100d)/100 + "<" + (double)Math.round(other.getLatitud()* 100d)/100);
		//System.out.println((double)Math.round(this.getLatitud()* 100d)/100<(double)Math.round(other.getLatitud()* 100d)/100);
		//System.out.println((double)Math.round(this.getLongitud()* 100d)/100 +">"+ (double)Math.round(other.getLongitud()* 100d)/100);
		//System.out.println((double)Math.round(this.getLongitud()* 100d)/100 > (double)Math.round(other.getLongitud()* 100d)/100);
		//System.out.println((double)Math.round(this.getLongitud()* 100d)/100 + "<" +(double)Math.round(other.getLongitud()* 100d)/100);
		//System.out.println((double)Math.round(this.getLongitud()* 100d)/100<(double)Math.round(other.getLongitud()* 100d)/100);
		if ( (double)Math.round(this.getLatitud()* 1000d)/1000 > (double)Math.round(other.getLatitud()* 1000d)/1000)
			return 1;
		else if ( (double)Math.round(this.getLatitud()* 1000d)/1000<(double)Math.round(other.getLatitud()* 1000d)/1000)
			return -1;
		else
		{
			if( (double)Math.round(this.getLongitud()* 1000d)/1000 > (double)Math.round(other.getLongitud()* 1000d)/1000)
				return 1;
			else if( (double)Math.round(this.getLongitud()* 1000d)/1000<(double)Math.round(other.getLongitud()* 1000d)/1000)
				return -1;
			else
				return 0;
		}

	}
}
