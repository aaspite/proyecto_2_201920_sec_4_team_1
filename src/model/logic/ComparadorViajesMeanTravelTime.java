package model.logic;

import java.util.Comparator;

public class ComparadorViajesMeanTravelTime implements Comparator<Viaje>{
		
		public ComparadorViajesMeanTravelTime(){
			
		}

		@Override
		public int compare(Viaje v1, Viaje v2) {
			
			if(v1.getMeanTravelTime() < v2.getMeanTravelTime()){
				return 1;
			} else if(v1.getMeanTravelTime() > v2.getMeanTravelTime()){
				return -1;
			} else if(v1.getMeanTravelTime() < v2.getMeanTravelTime()){
				return 1;
			} else if(v1.getMeanTravelTime() > v2.getMeanTravelTime()){
				return -1;
			} else {
				return 0;
			}
}
}
