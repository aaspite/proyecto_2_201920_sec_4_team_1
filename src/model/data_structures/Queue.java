package model.data_structures;

import java.util.Iterator;

public class Queue<E> implements IQueue<E>{

	//---------
	//Atributos
	//---------
	/**
	 * referencia a la cabeza de la cola
	 */
	private Node<E> head;


	/**
	 * referencia a la cola de la cola XD
	 */
	private Node<E> tail;
	//---------
	//Metodos
	//---------

	/**
	 * creacion de la cola
	 */
	public Queue() {
		head = null;
		tail = null;
	}
	@Override
	public Iterator<E> iterator() {
		return new iterator<E>(head); 
	}


	public boolean isEmpty() {
		// TODO
		return head == null;
	}

	@Override
	public int size() {
		int count = 0;
		Node<E> p = head;
		while (p != null) {
			count++	;
			p = p.getNext();
		}
		return count;
	}

	@Override
	public void enqueue(E t) {
		Node<E> node = new Node<E>(t);		


		if(isEmpty()) {
			head = node;
			tail = node;
			return;
		}		
		tail.setNext(node);
		tail = node;
	}

	@Override
	public E dequeue() {
		if(isEmpty()) {
			return null;
		}
		E resp = head.getData();
		if(head.getNext() == null) {
			head = null; 
			tail = null;
			return resp;
		}
		Node<E> p = head;
		head = head.getNext();
		p.setNext(null);
		return resp;
	}

	/**
	 * retorno de la cabeza
	 * @return cabeza
	 */
	public Node<E> getHead()
	{
		return head;
	}

	/**
	 * retorna la cola
	 * @return cola
	 */
	public Node<E> getTail()
	{
		return tail;
	}
}
