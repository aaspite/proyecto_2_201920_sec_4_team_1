package model.data_structures;

public interface IQueue<E> extends Iterable<E>{

	/**
	 * encola a un elemento E
	 * @param element, elemento a encolar
	 */
	public void enqueue(E element);
	
	/**
	 * elimina el primer elemento de la cola
	 * @return elemento eliminado
	 */
	public E dequeue();
	
	/**
	 * tama�o de la cola
	 * @return tama�o de la cola
	 */
	public int size();
}
