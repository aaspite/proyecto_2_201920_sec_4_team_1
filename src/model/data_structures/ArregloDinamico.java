package model.data_structures;


/**
 * 2019-01-23
 * Estructura de Datos Arreglo Dinamico de Strings.
 * El arreglo al llenarse (llegar a su maxima capacidad) debe aumentar su capacidad.
 *
 */
public class ArregloDinamico<E> implements IList<E>
{
	private E[] array;

	private static int  DEFAULT_SIZE = 50000;

	private int listSize;
	public ArregloDinamico() 
	{
		array = (E[])new Object[DEFAULT_SIZE];
	}
	@Override
	public void addFirst(E item) {

		if(listSize== array.length)
		{
			expandArray();
		}
		for(int i =listSize;i>0;i--)
		{
			array[i]= array[i-1];
		}
		array[0]=item;
		listSize++;
	}

	private void expandArray() {
		// TODO Auto-generated method stub
		E[] expanded =(E[])new Object[DEFAULT_SIZE+listSize];
		for(int i=0; i<array.length;i++)
		{
			expanded[i] = array[i];
		}
		array = null;
		array = expanded;
	}
	
	@Override
	public void replace(E item, int pos) {
		// TODO Auto-generated method stub
		if(listSize==array.length)
		{
			expandArray();
		}
		array[pos] =  item;
		//listSize++;
	}


	@Override
	public void append(E item) {
		// TODO Auto-generated method stub
		if(listSize==array.length)
		{
			expandArray();
		}
		array[listSize]=  item;
		listSize++;
	}
	@Override
	public void removeFirst() {
		// TODO Auto-generated method stub
		remove(0);

	}
	@Override
	public void remove(int pos) {
			if (pos == listSize-1) {
			array[pos] = null;
			}
			else {
			for (int i=pos; pos< listSize-1; i++) {
			array[i] = array[i+1];
			}
			array[listSize-1] = null;
			}
			listSize--;
	}
	@Override
	public E get(int pos) {
			return array[pos]; 

	}
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return listSize;
	}
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size()==0;

	}
}