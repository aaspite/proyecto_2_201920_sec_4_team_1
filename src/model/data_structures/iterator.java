package model.data_structures;


import java.util.Iterator;

public class iterator<E> implements Iterator<E>{
	//---------
	//Atributos
	//---------
	/**
	 * nodo a iterar
	 */
	private Node<E> node;

	//---------
	//Metodos
	//---------
	/**
	 * constructor del iterador
	 * @param node, raiz donde se va a empezar a iterar
	 */
	public iterator(Node<E> node)
	{
		this.node=node;
	}
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return node!=null;
	}

	@Override
	public E next() {
		// TODO Auto-generated method stub
		E data = node.getData();
		node = node.getNext();
		return data;
	}

}